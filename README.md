gitlab page https://gitlab.com/userAA/react-redux-demo-2-gitlab-english-comments.git
gitlab comment react-redux-demo-2-gitlab-english-comments

Illustration of the work redux, store, mapStateToProps, mapDispatchToProps

Technologies used:
    axios,
    react,
    react-dom,
    react-redux,
    react-scripts,
    redux,
    redux-logger,
    redux-thunk;
