//pulling out the cupcake purchase label
import { BUY_CAKE } from "./cakeTypes";

//setting the initial state of the number of cupcakes in the store store
const initialState = {
    numOfCakes: 10
}

const cakeReducer = (state = initialState, action) => {
    switch (action.type)
    {
        //we carry out the cupcakes (action.payload of cupcakes we take from the store)
        case BUY_CAKE:
        {
            return {
                ...state,
                numOfCakes: state.numOfCakes - action.payload
            }
        }

        default: return state;
    }
}

export default cakeReducer;