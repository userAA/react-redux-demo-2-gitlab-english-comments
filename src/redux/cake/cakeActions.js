//pulling out the cupcake purchase label
import { BUY_CAKE } from "./cakeTypes"

//the function of making a cupcake purchase
export const buyCake = (number = 1) => {
    return {
        type: BUY_CAKE,
        payload: number
    }
}