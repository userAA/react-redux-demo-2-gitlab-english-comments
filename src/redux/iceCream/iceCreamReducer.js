import { BUY_ICECREAM } from "./iceCreamTypes";

//setting the initial state of the number of ice cream packs in the store
const initialState = {
    numOfIceCreams: 20
}

const iceCreamReducer = (state = initialState, action) => {
    switch (action.type) {
        //we carry out the purchase of ice cream (we take one pack of ice cream from the store)
        case BUY_ICECREAM: return {
            ...state,
            numOfIceCreams: state.numOfIceCreams - 1
        }

        default: return state;
    }
}

export default iceCreamReducer;