//we pull out the label of the purchase of a pack of ice cream
import { BUY_ICECREAM } from "./iceCreamTypes";

//the function of making the purchase of ice cream
export const buyIceCream = () => {
    return {
        type: BUY_ICECREAM
    }
}