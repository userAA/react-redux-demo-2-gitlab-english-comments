import { combineReducers } from "redux";
//we pull out the cupcake editor reducer
import cakeReducer from './cake/cakeReducer';
//we pull out the ice сream editor reducer
import iceCreamReducer from "./iceCream/iceCreamReducer";

//формируем общий редюсер
const rootReducer = combineReducers({
    cake: cakeReducer,
    iceCream: iceCreamReducer
})

export default rootReducer;