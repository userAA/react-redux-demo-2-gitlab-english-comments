import {createStore, applyMiddleware} from 'redux';
import thunk from 'redux-thunk';
import {composeWithDevTools} from 'redux-devtools-extension'

import logger from 'redux-logger'
//we pull out the general reducer
import rootReducer from './rootReducer'

//we form the store repository based on the existing shared producer rootReducer 
const store = createStore(rootReducer, composeWithDevTools( applyMiddleware(logger, thunk)));

export default store;