import React from 'react';
import { Provider } from 'react-redux';
import store from './redux/store';
import './App.css';
import CakeContainer from './components/CakeContainer';
import HooksCakeContainer from './components/HooksCakeContainer';
import IceCreamContainer from './components/IceCreamContainer';
import NewCakeContainer from './components/NewCakeContainer';
import ItemContainer from './components/ItemContainer';

//project component
function App() {
  return (
    <Provider store={store}>
      <div className="App">
        {/*A component of the purchase of a single product (in this case, a cupcake), which uses the connect function */}
        <ItemContainer cake/>
        {/*A component of the purchase of a single product (in this case, a pack of ice cream), which uses the connect function */}
        <ItemContainer/>
        {/*A component of the purchase of a single product (in this case, a cupcake), which is implemented using useDispatch and useSelector */}
        <HooksCakeContainer/>
        {/*The cupcake purchase component, which is implemented using the connect function */}
        <CakeContainer/>
        {/*The component of buying a pack of ice cream, which is implemented using the connect function*/}
        <IceCreamContainer/>
        {/*The component of buying a certain number of cupcakes, which is implemented using the connect function */}
        <NewCakeContainer/>
      </div>
    </Provider>
  );
}

export default App;
