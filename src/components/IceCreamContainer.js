import React from 'react'
import {connect} from 'react-redux'
//we pull out the function of buying a pack of ice cream
import { buyIceCream } from '../redux'

//the component of showing the number of ice cream packs in the store
function IceCreamContainer(props)
{
    return (
        <div>
            {/*Number of ice cream packs in the storage */}
            <h2>Number of icecreams - {props.numOfIceCreams}</h2>
            {/*The button for buying a pack of ice cream*/}
            <button onClick={props.buyIceCream}>Buy IceCream</button>
        </div>
    )
}

const mapStateToProps = state => {
    //we output the number of remaining packs of ice cream after the next purchase
    return {
        numOfIceCreams: state.iceCream.numOfIceCreams
    }
}

//We make the purchase of a pack of ice cream
const mapDispatchToProps = dispatch => {
    return {
        buyIceCream: () => dispatch(buyIceCream())
    }
}

//using the connect function, we contact with the storage
export default connect(mapStateToProps, mapDispatchToProps)(IceCreamContainer);