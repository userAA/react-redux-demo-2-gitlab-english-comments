import React from 'react';
import {connect} from "react-redux"
//we pull out the functions of buying goods either a cupcake or a pack of ice cream
import { buyCake, buyIceCream } from '../redux';

//the component of showing the number of products in the storage
function ItemContainer(props) {
    return (
        <div>
            {/*Showing the number of remaining products*/}
            <h2>Item - {props.item}</h2>
            {/*Product purchase button*/}
            <button onClick={props.buyItem}>Buy Items</button>
        </div>
    )
} 

const mapStateToProps = (state, ownProps) => 
{
    //we display the number of remaining products after the next purchase (it's either cupcakes or packs of ice cream)
    const itemState = ownProps.cake ? state.cake.numOfCakes : state.iceCream.numOfIceCreams;

    return {
        item : itemState
    }
}

const mapDispatchToProps = (dispatch, ownProps) => 
{
    //We make either the purchase of a cupcake or a pack of ice cream
    const dispatchFunction = ownProps.cake ? () => dispatch(buyCake()) : () => dispatch(buyIceCream())

    return {
        buyItem : dispatchFunction
    }
}

//using the connect function, we contact the with storage
export default connect(mapStateToProps, mapDispatchToProps) (ItemContainer);