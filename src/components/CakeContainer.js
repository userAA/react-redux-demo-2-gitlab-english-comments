import React from 'react'
import {connect} from 'react-redux'
//pulling out the cupcake purchase function
import { buyCake } from '../redux'

//component of displaying the number of cupcakes in the storage and buying one cupcake
function CakeContainer(props)
{
    return (
        <div>
            {/*Number of cupcakes in storage */}
            <h2>Number of cakes - {props.numOfCakes}</h2>
            {/*Cupcake purchase button*/}
            <button onClick={props.buyCake}>Buy Cake</button>
        </div>
    )
}

const mapStateToProps = state => {
    //we output the number of remaining cupcakes after the next purchase
    return {
        numOfCakes: state.cake.numOfCakes
    }
}

//We make the purchase of a cupcake
const mapDispatchToProps = dispatch => {
    return {
        buyCake: () => dispatch(buyCake())
    }
}

//using the connect function, we contact wit store
export default connect(mapStateToProps, mapDispatchToProps)(CakeContainer);