import React, {useState} from 'react'
import {connect} from 'react-redux'
import { buyCake } from '../redux'

//the component of displaying the number of cupcakes in the store and buying an arbitrary number of cupcakes
function NewCakeContainer(props)
{
    //состояние количества покупаемых кексов
    //status of the number of cupcakes purchased
    const [number, setNumber] = useState(1);
    return (
        <div>
            {/*Количество кексов, оставшихся в хранилище store */}
            {/*Number of cupcakes remaining in the storage */}
            <h2>Number of cakes - {props.numOfCakes}</h2>
            {/*Setting the number of cupcakes we want to buy */}
            <input type="text" value={number} onChange={e => setNumber(e.target.value)} />
            {/*Кнопка покупки заданного  количества кексов*/}
            {/*The button for buying a specified number of cupcakes*/}
            <button onClick={() => props.buyCake(number)}>Buy {number} Cakes</button>
        </div>
    )
}

const mapStateToProps = state => {
    //we output the number of remaining cupcakes after the next purchase
    return {
        numOfCakes: state.cake.numOfCakes
    }
}

//We make the purchase of a cupcake
const mapDispatchToProps = dispatch => {
    return {
        buyCake: number => dispatch(buyCake(number))
    }
}

//using the connect function, we contact with the store 
export default connect(mapStateToProps, mapDispatchToProps)(NewCakeContainer);