import React from "react";
//pulling out the useSelector and useDispatch hooks
import { useSelector, useDispatch } from "react-redux";
//pulling out the cupcake purchase function
import { buyCake } from "../redux";

//the cupcake purchase component (the operation is carried out through hooks)
function HooksCakeContainer() {
    //we pull out the number of remaining cupcakes in the storage using useSelector hook
    const numOfCakes = useSelector(state => state.cake.numOfCakes);
    const dispatch = useDispatch();

    return (
        <div>
            {/*showing the number of remaining cupcakes in the store */}
            <h2>Num of cakes - {numOfCakes}</h2>
            {/*cupcake purchase button */}
            <button onClick={() => dispatch(buyCake())}>Buy cake</button>
        </div>
    )
}

export default HooksCakeContainer;